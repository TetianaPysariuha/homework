class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age =age;
        this._salary = salary;
    }

    get name () {
        return this._name;
    }

    set name (someName) {
        if (someName.trim() === '') {
            alert('New value of Name contains nothing');
        } else {
            this._name = someName;
        }
    }

    get age () {
        return this._age;
    }
    
    set age (someAge) {
        if (isNaN(+someAge)) {
            alert('New value of age is not a number');
        } else if (+someAge < 0 || +someAge > 100){
            alert(`A person with age ${someAge} cannot be our employee`);
        } else {
            this._age = +someAge;
        } 
    }

    get salary () {
        return this._salary;
    }

    set salary (somesalary) {
        if (isNaN(+somesalary)) {
            alert('New value of salary is not a number');
        } else {
            this._salary = +somesalary;
        } 
    }
}

class Programmer extends Employee {
    constructor (lang, name, age, salary) {
        super (name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang (langList) {
        this._lang = langList
    }

    get salary () {
        return this._salary * 3;
    }
}

console.log (new Programmer(['JS'], 'John', 32, 55000));
console.log (new Programmer(['C++'], 'Boby', 40, 55000));
console.log (new Programmer(['C++', 'C#'], 'Mike', 27, 25000));
