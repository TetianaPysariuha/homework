const findIpLink = "https://api.ipify.org/?format=json";
const findAdressByIpLink = "http://ip-api.com/json/";
const textContainer = document.querySelector('#find-computer__informantion');

function insertAdressInformation(container, someOneLayerObject){
    container.innerHTML = Object.entries(someOneLayerObject).map(elem =>`<p><span>${elem[0]}:</span> ${elem[1]}</p>`).join('');
};

async function getIPAndAdress () {
    textContainer.innerHTML= `<p>Чекай хвильку... </p><p>Я шукаю ... </p>`;
    try{
        const {ip} = await fetch(findIpLink).then(response => response.json());
        const {timezone, country, region, regionName, city} = await fetch(findAdressByIpLink + ip).then(response => response.json());
        const continent = timezone.split('/')[0];
        insertAdressInformation(textContainer, {ip, continent, country, region, regionName, city});
    }
    catch(err){
        textContainer.innerHTML= `<p>Вибач, але щось пішло не так </p><p>Спробуй трошки пізніше </p>`;
    }
}

document.querySelector('#find-computer__button').addEventListener('click', getIPAndAdress);


