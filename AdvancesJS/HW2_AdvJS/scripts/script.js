const conteinerDiv = document.querySelector('#root');

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  class InvalidElementError extends Error{
    constructor (message) {
      super();
      this.name = 'InvalidElementError';
      this.message = message;
    }
  }

  class List {
    constructor (container, someArr, drawElemFunc) {
      this._container = container;
      this._arrayForList = someArr;
      this.drawElemList = drawElemFunc;
    }

    drawList() {
      const ulList = document.createElement('ul');
      this._arrayForList.forEach((element, index) => {
        try {
          ulList.insertAdjacentHTML('beforeend', this.drawElemList(element));
        }
        catch(InvalidElementError) {
          console.log(`An error occurred with the ${index} element of array: 
${InvalidElementError.message}
The element is`);
          console.log(element);
        }
      });
      this._container.append(ulList);
    } 
  };

function drawBookElement (bookObject) {
  if (!bookObject.author){
    throw new InvalidElementError("The property author does not exist");
  } else if (!bookObject.name) {
    throw new InvalidElementError("The property name does not exist");
  } else if (!bookObject.price) {
    throw new InvalidElementError("The property price does not exist");
  }
  return `<li class="list-item">
  <p><span class="list-item-category">Автор книги: </span class= "list-item-value"><span>${bookObject.author}</span></p>
  <p><span class="list-item-category">Найменування книги: </span class= "list-item-value"><span>${bookObject.name}</span></p>
  <p><span class="list-item-category">Ціна: </span class= "list-item-value"><span>${bookObject.price}</span></p>
</li>`
}

new List(conteinerDiv, books, drawBookElement).drawList();
