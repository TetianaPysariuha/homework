const listConteiner = document.querySelector('#movie-list');
const filmsLink = 'https://ajax.test-danit.com/api/swapi/films';

function drawList(container, someArr, drawElemFunc) {
        someArr.forEach((element) => {
        try {
            container.append(drawElemFunc(element));
        }
        catch(err) {
          console.log(`An error occurre: ${err.message} The element is`);
          console.log(element);
        }
      });
    };

  function isLink(someString){
    if(typeof someString === 'string' && someString.startsWith('http')) {
        return true
    } else {
        return false
    };
  };

function drawFilmListElement(filmObject){
    const {episodeId, name, openingCrawl} = filmObject;
    const liElem = document.createElement('li');
    liElem.classList.add('movie-list_item');
    liElem.insertAdjacentHTML('beforeend', 
    `<h2 class="movie-list_item-episode">Episode ${episodeId}. ${name}</h2>
    <p class="movie-list_item-description">${openingCrawl}</p>
    <h3 class="movie-list_item-title">Characters:</h3>`);
    return liElem;
};

function drawCharacterListElement(characterObject) {
    const {name, id, ...other} =  characterObject;
        const carsctLi = document.createElement('li');
        carsctLi.classList.add('movie-list_item-characters_item');
        carsctLi.insertAdjacentHTML('beforeend', `<p class="movie-list_item-characters_name">${name}</p>`);
            
        Object.entries(other).forEach(element => {
            const pair = document.createElement('p');
            pair.insertAdjacentHTML('beforeend', `<span class="span-title">${element[0]}: </span>`);
            if (Array.isArray(element[1]) && element[1].length > 0) {
                element[1].forEach(value => {
                    if(isLink(value)) {
                        pair.insertAdjacentHTML('beforeend', `<a href="${value}" target="_blank">${value}</a>`)
                    } else {
                        pair.insertAdjacentHTML('beforeend', `<p>${value}</p>`)
                    }
                })
            } else {
                if(isLink(element[1])) {
                    pair.insertAdjacentHTML('beforeend', `<a href="${element[1]}" target="_blank">${element[1]}</a>`)
                } else {
                    pair.insertAdjacentHTML('beforeend', `<span>${element[1]}</span>`)
                }};
            carsctLi.append(pair);
            })
           return carsctLi;
};


fetch(filmsLink)
.then(response => response.json())
.then(result => {
    result.forEach(element => {
        const newListElem = drawFilmListElement(element);
        const charactersList = document.createElement('ul');
        charactersList.classList.add("movie-list_item-characters");
        listConteiner.append(newListElem);
        newListElem.append(charactersList);
        element.characters.forEach(elem =>
        fetch(elem)
        .then(response => response.json())
        .then(res => {
            charactersList.append(drawCharacterListElement(res));
        }))});
    });
