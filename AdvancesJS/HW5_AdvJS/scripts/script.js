const usersLink = 'https://ajax.test-danit.com/api/json/users';
const postsLink = 'https://ajax.test-danit.com/api/json/posts';
const postsContainer = document.querySelector('#content-posts');

class PostCard {
    constructor(authorName, authorMail, contentTitle, contentText, id, deleteFunction) {
        this._authorName = authorName,
        this._authorMail = authorMail,
        this._contentTitle = contentTitle,
        this._contentText = contentText,
        this._id = id,
        this.deleteFunction = deleteFunction
    }

    drowCard(){
        const newCard = document.createElement('div');
        newCard.classList.add('content-posts__post');
        newCard.dataset.id = this._id;
        newCard.insertAdjacentHTML('beforeend', `
        <div class="content-posts__post-author">
            <p class="content-posts__post-author__name">${this._authorName}</p>
            <a class="content-posts__post-author__mail" href="mailto:${this._authorMail}">${this._authorMail}</a>
        </div>
        <p class="content-posts__post-title">${this._contentTitle}</p>
        <p class="content-posts__post-text">${this._contentText}</p>`);
        const newDelButton = document.createElement('button');
        newDelButton.classList.add('content-posts__post-delete');
        newDelButton.innerHTML = 'X';
        newDelButton.addEventListener('click', (event) => this.deleteFunction(event.target.parentElement, event.target.parentElement.dataset.id));
        newCard.append(newDelButton);
        return newCard;
    }
}

function postDelete(domElement, id){
    fetch(postsLink+'/'+id, {
        method: 'DELETE'
    })
    .then(({ok}) => {
        console.log(ok);
        if (ok) {
            domElement.remove();
        } else (
            alert('Some problems with deleting the element. Please, try to delete later.')
        );
    })
    .catch(err => alert('Some problems with deleting the element. Please, try to delete later.'));
}

fetch(usersLink)
.then(responseUsers => responseUsers.json())
.then(responseUsers =>  {
    fetch(postsLink)
    .then(responsePosts => responsePosts.json())
    .then(responsePosts => {
        const commonArray = responsePosts.map(post => {
            const filteredUser = responseUsers.filter(user => user.id === post.userId);
            post.authorName = filteredUser[0]?.name;
            post.authorMail = filteredUser[0]?.email;
            return post;
        })
        commonArray.forEach(({authorName = 'anonimus', authorMail = 'no email', title, body, id}) => postsContainer.append(new PostCard(authorName, authorMail, title, body, id, postDelete).drowCard()))
    });
    }
).catch(err => postsContainer.insertAdjacentHTML('beforeend', `<p class="error-fetch">OOOPPS! Something went wrong :( </p>`));

