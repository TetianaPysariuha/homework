import React from 'react';
import Container from '../../components/Container/Container';
import styles from "./MainPage.module.scss";
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';


function MainPage ({sectionCardTitle}) {

    const dataCards = useSelector(store => store.dataCards.data); 

    return (
        <div className={styles.mainPage}>
            <Container sectionCardTitle = {sectionCardTitle} dataCards = {dataCards}/>
        </div>
    )
}

MainPage.propTypes = {
    sectionCardTitle: PropTypes.string
}

MainPage.defaultProps = {
    sectionCardTitle: ''
}

export default MainPage;