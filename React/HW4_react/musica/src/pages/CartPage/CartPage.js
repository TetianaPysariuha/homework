import React from 'react';
import styles from "./CartPage.module.scss" ;
import CartContainer from '../../components/CartContainer/CartContainer';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

function CartPage () {

    const cartData = useSelector(store => store.cartData.cartData); 

    return (
    <div className={styles.cartPage}>
       <h1>Кошик</h1>
       <CartContainer cartData={cartData}
                    noElementText={'Ви ще не вибрали жодного товару'}/>
    </div>
    )
}

CartPage.propTypes = {

}

CartPage.defaultProps = {

}

export default CartPage;