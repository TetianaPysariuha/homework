import React from "react";
import styles from "./CurtItem.module.scss";
import Img from '../Img/Img';
import PropTypes from 'prop-types';
import Button from "../Button/Button";

class CurtItem extends React.PureComponent {
    render(){
        const { url, title, color, count, art, handleClickDelFromCart} = this.props;
        return(
            <div className={styles.cutrItem}>
                <Img url = {url} alt = {title}/>
                <span>{title} {color}</span>
                <span>{count}</span>
                <Button btnText= "delete" handleClick = {()=> {handleClickDelFromCart(art)}} backgroundColor = "rgba(0, 128, 50, 0.8)" color = "white"/>
            </div>
        )
    }
}


CurtItem.propTypes = {
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired, 
    art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
    color: PropTypes.string, 
    handleClickDelFromCart: PropTypes.func,
    count: PropTypes.number
}

CurtItem.defaultProps = {
    color: null,
    count: 0,
    handleClickDelFromCart: () => {}
}

export default CurtItem;