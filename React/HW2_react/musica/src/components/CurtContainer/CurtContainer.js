import React from "react";
import styles from "./CurtContainer.module.scss";
import CurtItem from "../CurtItem/CurtItem"
import PropTypes from 'prop-types';

class CurtContainer extends React.PureComponent {
    render(){
        const {curtData, handleClickDelFromCart} = this.props;
        return(
            <div className={styles.curtContainer}>
                {curtData.map(({imgSrc, name, color, count, art}) => <CurtItem  key = {art} url={imgSrc} title ={name} color ={color} count = {count} art= {art} handleClickDelFromCart={handleClickDelFromCart}/>)}
            </div>
        )
    }
}

CurtContainer.propTypes = {
    curtData: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired,
    handleClickDelFromCart: PropTypes.func
}

CurtContainer.defaultProps = {
    sectionCardTitle: '',
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: '',
        price: null,
        color: null, 
        isFavorite: false,
        count: 0
    })),
    handleClickAddToCart: () => {}
}

export default CurtContainer;