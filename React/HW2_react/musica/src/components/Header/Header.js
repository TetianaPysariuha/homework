import React from "react";
import styles from "./Header.module.scss";
import Img from '../Img/Img';
import CurtContainer from "../CurtContainer/CurtContainer"
import PropTypes from 'prop-types';

class Header extends React.PureComponent {
    
    render(){
        const {curtData, counterCurt, counterFavorite, handleClickDelFromCart, showCurt, isCurtAvaible} = this.props;
        return(
            <header className={styles.header}>
                <div className={styles.logo}>
                    <Img url = "./img/Logo.svg" alt = "Logo"/>
                    <a href="#">CHAIRS</a>
                </div>
                {isCurtAvaible && counterCurt != 0 && <CurtContainer curtData={curtData} handleClickDelFromCart={handleClickDelFromCart} />}
                <div className={styles.statistic}>
                    <div onClick = {showCurt} className={styles.curt}>
                        <Img url = "./img/cart-outline.svg" alt = "Curt"/>
                        <p>{counterCurt}</p>
                    </div>
                    <div className={styles.favorite}>
                        <Img url = "./img/star-header.svg" alt = "favorite"/>
                        <p>{counterFavorite}</p>
                    </div>
                </div>
            </header>
        )
    }
}

CurtContainer.propTypes = {
    curtData: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired,
    counterCurt: PropTypes.number,
    counterFavorite: PropTypes.number,
    handleClickDelFromCart: PropTypes.func,
    showCurt: PropTypes.func,
    isCurtAvaible: PropTypes.bool
}

CurtContainer.defaultProps = {
    sectionCardTitle: '',
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: '',
        price: null,
        color: null, 
        isFavorite: false,
        count: 0
    })),
    handleClickAddToCart: () => {},
    counterCurt: 0,
    counterFavorite: 0,
    showCurt: () => {},
    isCurtAvaible: false
}


export default Header;