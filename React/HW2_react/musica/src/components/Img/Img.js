import React from "react";
import PropTypes from 'prop-types'

class Img extends React.PureComponent {

    render () {
        const {url, alt} = this.props;
        return <img src={url} alt = {alt}/>
    }
}

Img.propTypes = {
    url: PropTypes.string.isRequired,
    alt: PropTypes.string
}

Img.defaultProps = {
    alt: "An Image"
}

export default Img;