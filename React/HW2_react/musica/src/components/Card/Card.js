import React from "react";
import styles from './Card.module.scss';
import Img from '../Img/Img';
import PropTypes from 'prop-types';
import Button from "../Button/Button";


class Card extends React.PureComponent {

    render () {
        const {title, price, url, art, color, isFavorite, handleClickAddToCart, handleClickOnFavorite} = this.props;
        return (
            <div className={styles.card}>
                <div className={styles.imgMain}>
                    <Img url = {url} alt = {title}/>
                </div>
                <div className={styles.imgFavorite} onClick = {(event)=> {event.stopPropagation(); handleClickOnFavorite(art)}}>
                    <Img url = {isFavorite ? "./img/star_remove.png" : "./img/star_add.png"} alt = {isFavorite ? "Remove from favorite" : "Add to favorite"} />
                </div>
                <h3>{title}</h3>
                <span>{color}</span>
                <span>Арт.: {art}</span>
                <p className={styles.prise}>{price}</p>
                <Button btnText= "Додати до кошика" handleClick = {()=> {handleClickAddToCart({title, color, art})}} backgroundColor = "rgba(0, 128, 50, 0.8)" color = "white"/>
            </div>
        )
    }
}

Card.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired, 
    url: PropTypes.string.isRequired, 
    art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool,
    handleClickCard: PropTypes.func,
    handleClickOnFavorite: PropTypes.func
}

Card.defaultProps = {
    color: null,
    isFavorite: false,
    handleClickCard: () => {},
    handleClickOnFavorite: () => {}
}

export default Card;