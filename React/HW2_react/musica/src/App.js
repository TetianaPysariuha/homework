import React from 'react';
import './App.css';
import Container from './components/Container/Container';
import Modal from './components/Modal/Modal'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'


class App extends React.PureComponent {
  
  state = {
    data: [],
    curtData: [],
    isOpenModal: false,
    artForAddToCurt: null,
    elForAddToCurt: {},
    counterCurt: 0,
    counterFavorite: 0,
    isCurtAvaible: false
  }
  
  async componentDidMount () {
    if (localStorage.getItem('data')){
      this.setState((current) => {
        const newData = JSON.parse(localStorage.getItem('data'));
        const counterFavorite = newData.filter((el)=> el.isFavorite).length
        return {data: newData, counterFavorite: counterFavorite};
      });
    } else {
      const newData = await fetch('./data.json').then(res => res.json());
      this.setState((current) => {
        const newCurr = {...current};
        newCurr.data = [...newData];
        return newCurr;
      });
      localStorage.setItem('data', JSON.stringify(newData));
    };

    if (localStorage.getItem('curtData')){
      const newData = JSON.parse(localStorage.getItem('curtData'));
      this.setState((current) => {
        const newCurr = {...current};
        newCurr.curtData = [...newData];
        return newCurr;
      });
  }
}
  
  showCurt = () => {
    this.setState((cur) => {
        return {isCurtAvaible: (cur.isCurtAvaible ? false : true)};
    })
}

  handleClickAddToCart = (card) =>{
    this.setState({isOpenModal: true, artForAddToCurt: card.art, elForAddToCurt: card});
    if(this.state.isCurtAvaible){
      this.showCurt();
    }
  }

  closeModal = () =>{
    this.setState({isOpenModal: false});
  }

  confirmModalHandle = (art) => {
    this.setState((current) => {
      const curtData = [...current.curtData];
      const index = this.getElIndexByArt(curtData, art);
      if (index === -1) {
        const currData = [...current.data];
        curtData.push({...currData[this.getElIndexByArt(currData, art)], count: 1});
      } else {
        curtData[index].count += 1;
      }
      return {curtData: curtData, artForAddToCurt: null};
    });
  }
  
  handleClickDelFromCart = (art) =>{
    this.setState((current) => {
      const curtData = [...current.curtData];
      const index = this.getElIndexByArt(curtData, art);
      if (index === -1) {
        return;
      } else if (curtData[index].count > 1) {
        curtData[index].count -= 1;
      } else{
        curtData.splice(index, 1);
      }
      return {curtData: curtData};
    });
  }

  handleClickOnFavorite = (art) => {
    this.setState((current) =>{
      const newData = [...current.data];
      const elemIndex = this.getElIndexByArt(newData, art);
      newData[elemIndex].isFavorite = (newData[elemIndex].isFavorite ? false : true);
      const counterFavorite = current.counterFavorite + (newData[elemIndex].isFavorite ? +1: -1)
      return {data: newData, counterFavorite: counterFavorite};
    })
    if(this.state.isCurtAvaible){
      this.showCurt();
    }
  }

  getElIndexByArt = (arrData, art) =>{
    return arrData.findIndex((element) => element.art === art)
  }

  componentDidUpdate() {
    localStorage.setItem('data', JSON.stringify(this.state.data));
    localStorage.setItem('curtData', JSON.stringify(this.state.curtData));
    this.setState((curr) => {
      let counterCurt;
      if(curr.curtData.length > 0){
        let init = 0;
        counterCurt = curr.curtData.reduce((acc, cur)=>acc + cur?.count, init);
      }else{
        counterCurt = 0;
      };
      return{counterCurt: counterCurt};
    })
  }

  render () {
    const {curtData, data, isOpenModal, elForAddToCurt, counterCurt, counterFavorite, isCurtAvaible} = this.state;
    return (
      <div className="App">
      {isOpenModal && <Modal header = "Додати до кошика?" 
                                    closeButton = {false} 
                                    text = {"Товар: " + elForAddToCurt?.title + " (" + elForAddToCurt?.color +") " } 
                                    closeModaleHandle = {this.closeModal}
                                    confirmModalHandle = {this.confirmModalHandle}
                                    art = {this.state.artForAddToCurt}>
                      </Modal>}
      <Header curtData = {curtData} counterCurt = {counterCurt} counterFavorite = {counterFavorite} handleClickDelFromCart = {this.handleClickDelFromCart} showCurt={this.showCurt} isCurtAvaible={isCurtAvaible}/>
      <Container sectionCardTitle = 'Розділ Меблі' dataCards = {data} handleClickAddToCart = {this.handleClickAddToCart} handleClickOnFavorite={this.handleClickOnFavorite}/>
      <Footer/>
      </div>
    );
  }
}

export default App;
