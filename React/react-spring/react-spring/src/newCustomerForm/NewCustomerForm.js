import React from "react";
import styles from "./NewCustomerForm.module.scss";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

const NewCustomerForm = () => {

    const handleSubmit = (values, { resetForm }) => {
        const dataSend = values;
        console.log(dataSend);
        (async () => {
                await fetch('http://localhost:9000/customers/', {
                     method: 'POST',
                     mode: 'cors',
                     headers: {
                       'Content-Type': 'application/json'
                     },
                     body: JSON.stringify(values)/*JSON.stringify({values}).then(res => res.json())*/
              })();
                  })();
        resetForm();
    }

    const initialValues = {
        name: '',
        email: '',
        age: '',
        phone:'',
        password: ''
    }

    const validationChema = yup.object().shape({
        name: yup.string()
            .required('Поле є обов\'язковим до заповнення'),
        email: yup.string()
            .required('Поле є обов\'язковим до заповнення'),
        age: yup.number()
            .required('Поле є обов\'язковим до заповнення'),
        phone: yup.string()
            .required('Поле є обов\'язковим до заповнення'),
        password: yup.string()
            .required('Поле є обов\'язковим до заповнення'),
        // confirmPassword: yup.string()
        //     .required('Поле є обов\'язковим до заповнення'),
    })

    return(<Formik
              initialValues={initialValues}
              validationSchema={validationChema}
              onSubmit={handleSubmit}

          >
              {({ isValid, dirty }) => (
                  <Form action="customers/"  method="post" className={styles.form}>

                      <Field
                          className={styles.formField}
                          name='name'
                          type='text'
                          placeholder = "Ім'я"
                      />
                      <ErrorMessage name='name'>{msg => <span className={styles.errorMessage}>{msg}</span>}</ErrorMessage>

                      <Field
                          className={styles.formField}
                          name='email'
                          type='email'
                          placeholder = "email"
                      />
                      <ErrorMessage name='email'>{msg => <span className={styles.errorMessage}>{msg}</span>}</ErrorMessage>

                      <Field
                          className={styles.formField}
                          name='age'
                          type='text'
                          placeholder = "Вік"
                      />
                      <ErrorMessage name='age'>{msg => <span className={styles.errorMessage}>{msg}</span>}</ErrorMessage>

                      <Field
                          className={styles.formField}
                          name='phone'
                          type='text'
                          placeholder = "Номер телефону"
                      />
                      <ErrorMessage name='phone'>{msg => <span className={styles.errorMessage}>{msg}</span>}</ErrorMessage>

                      <Field
                          className={styles.formField}
                          name='password'
                          type='text'
                          placeholder = "Пароль"
                      />
                      <ErrorMessage name='password'>{msg => <span className={styles.errorMessage}>{msg}</span>}</ErrorMessage>

                      {/* <Field
                          className={styles.formField}
                          name='confirmPassword'
                          type='text'
                          placeholder = "Підтвердження паролю"
                      />
                      <ErrorMessage name='confirmPassword'>{msg => <span className={styles.errorMessage}>{msg}</span>}</ErrorMessage> */}

                      <button
                          className={styles.btnSubmit}
                          disabled={!dirty || !isValid}
                          type='submit'

                      >Add customer</button>

                  </Form>)
              }
          </Formik>)

}

export default NewCustomerForm;
