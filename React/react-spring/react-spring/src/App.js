import './App.css';
import React, { useState, useEffect } from 'react';
import Header from './header/Header';
import Main from './main/Main';
import Navbar from './navbar/Navbar';
import Modal from './modal/Modal.js'

function App() {
  const [customers, setCustomers] = useState([]);
  const [currentCustomer, setCurrentCustomer] = useState({});
  const [isNeededRefresh, setIsNeededRefresh] = useState(false);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [modalContent, setModalContent] = useState(<></>);
  const [modalHeader, setModalHeader] = useState("");

  useEffect(() => {
      getAllCustomers();
  }, []);
  

  useEffect(() => {
 console.log("isNeededRefresh " + isNeededRefresh);

  if(isNeededRefresh){
      getAllCustomers();
  }
}, [isNeededRefresh]);

/*useEffect(() => {
  console.log(currentCustomer);
}, [currentCustomer]);*/

const onCustomerClick = (id) => {
  setCurrentCustomer(customers.filter(el => el.id === id)[0]);
}

const getAllCustomers = () => {
  (async () => {
        const newData = await fetch('http://localhost:9000/customers/').then(res => res.json());
        setCustomers(newData);
        setCurrentCustomer(newData[0]);
        console.log("newData " + newData);
      })();
      setIsNeededRefresh(false);
}

const onDeleteCustomerClick = (id) => {
  (async () => {
    await fetch(`http://localhost:9000/customers/${id}`, {
      method: 'DELETE',
      mode: 'cors'});
      setIsNeededRefresh(true);
  })();
}

const onDeleteAccountClick = (id, accountNumber) => {
  (async () => {
    await fetch(`http://localhost:9000/customers/${id}/account`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({"accountNumber" : accountNumber})}).then(res => res.json());
      setIsNeededRefresh(true);
  })();
}

const closeButton = () => {
    setIsOpenModal(false);
    setIsNeededRefresh(true);
}

const confirmModalHandle = () => {
    setIsOpenModal(false);
    setIsNeededRefresh(true);
}

const openModal = (content) => {
    setModalContent(content);
    setIsOpenModal(true);
}


  return (
    <div className="App">
     {isOpenModal && <Modal header={modalHeader}
                            content={modalContent}
                            closeButton={closeButton}
                            confirmModalHandle={confirmModalHandle}/>}
      <Header/>
      <Navbar/>
      <Main customers = {customers} 
            onClickHandler = {onCustomerClick}
            currentCustomer = {currentCustomer} 
            customerDelClickHandler={onDeleteCustomerClick}
            accountDelClickHandler={onDeleteAccountClick}
            openModal={openModal}
            setModalHeader={setModalHeader}
            />
    </div>
  );
}

export default App;
