import React from "react";
import styles from './Modal.module.scss';
import Button from "../button/Button";

function Modal (props) {
    const {header, content, closeButton, confirmModalHandle} = props;

    return <div className={styles.container}>
                <div className={styles.containerBackground}></div>
                <div className={styles.content}>
                    <div className={styles.modalHeader}>
                        <h2>{ header }</h2>
                        <Button handleClick = {closeButton}
                                                backgroundColor = 'rgba(0, 0, 0, 0.3)'
                                                color = "white" btnText = 'X'/>
                    </div>
                    {content}
                </div>
            </div>
    }


export default Modal;