import React from "react";
import styles from "./Customer.module.scss";

function Customer(props) {
    const {name, email, age, onClickHandler, currentCustomerId, id, customerDelClickHandler} = props;
    let currentStyle = styles.customer;
    if(currentCustomerId === id){
        currentStyle = styles.activeCustomer;
    }
    return(
        <div className={currentStyle} onClick={onClickHandler}>
            <div className={styles.customerInfo}>
                <div className={styles.name}><p>{name}</p></div>
                <div className={styles.customerData}>
                    <p>email: {email}</p>
                    <p>age: {age}</p>
                </div>
            </div>
            <button className={styles.addbutton} onClick={customerDelClickHandler}>Delete</button>
        </div>
    );
}

export default Customer;