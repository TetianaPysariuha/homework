import React from "react";
import styles from "./NewAccountForm.module.scss";
import { Formik, Form, Field, ErrorMessage } from 'formik';

const NewAccountForm = (props) => {
    const {currentCustomer} = props;

    const handleSubmit = (values, { resetForm }) => {
        const dataSend = values;
        console.log(dataSend);
        (async () => {
                const newData = await fetch(`http://localhost:9000/customers/${currentCustomer.id}/account`, {
                     method: 'POST',
                     mode: 'cors',
                     headers: {
                       'Content-Type': 'application/json'
                     },
                     body: JSON.stringify(values)//.then(res => res.json())
              })();
                  })();
        resetForm();
    }

    const initialValues = {
        currency: ' '
    }

    return(<Formik
              initialValues={initialValues}
              onSubmit={handleSubmit}

          >
              {({ isValid, dirty }) => (
                  <Form method="post" className={styles.form}>

                      <Field as="select" name="currency">
                          <option value="">Choose currency</option>
                          <option value="UAH">UAH</option>
                          <option value="USD">USD</option>
                          <option value="EUR">EUR</option>
                          <option value="CHF">CHF</option>
                          <option value="GBP">GBP</option>
                      </Field>

                      <button
                          className={styles.btnSubmit}
                          disabled={!dirty || !isValid}
                          type='submit'

                      >Add account</button>

                  </Form>)
              }
          </Formik>)

}

export default NewAccountForm;
