import React from "react";
import Account from "../account/Account.js";
import styles from "./AccountContainer.module.scss";
import NewAccountForm from "../newAccountForm/NewAccountForm.js";


function AccountContainer (props) {
    const {currentCustomer, accountDelClickHandler, openModal, setModalHeader} = props;
    console.log(currentCustomer.accounts);
    return(
        <div className={styles.accountContainer}>
            <div className={styles.accountHeader}>
                <h5>Accounts</h5>
                <button onClick={()=>{openModal(<NewAccountForm currentCustomer={currentCustomer}/>);
                                        setModalHeader("Open a new account for \n"+currentCustomer.name)}} className={styles.addbutton}>Add new</button>
            </div>
            <div>
                {currentCustomer !== undefined
                && currentCustomer.accounts !== undefined
                && currentCustomer.accounts.length > 0
                && currentCustomer.accounts.map(
                ({id, number, currency, balance}) =>
                    <Account key = {id}
                            number = {number}
                            currency = {currency}
                            balance = {balance}
                            accountDelClickHandler={() => accountDelClickHandler(currentCustomer.id, number)}/>)}
            </div>
        </div>
    );
}

export default AccountContainer;