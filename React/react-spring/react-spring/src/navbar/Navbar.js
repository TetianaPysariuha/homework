import React from "react";
import styles from "./Navbar.module.scss";

function Navbar() {
    return(
        <div className={styles.navbar}>
            <ul>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <hr/>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
                <hr/>
                <li><a href="#">Some point</a></li>
                <li><a href="#">Some point</a></li>
            </ul>
        </div>
    )
}

export default Navbar;