import React from "react";
import styles from "./Main.module.scss";
import CustomerContainer from "../customerContainer/CustomerContainer.js";
import AccountContainer from '../accountContainer/AccountContainer.js';
import { Formik, Form, Field, ErrorMessage } from 'formik';

function Main(props) {
    const { customers,
            currentCustomer,
            onClickHandler,
            customerDelClickHandler,
            accountDelClickHandler,
            openModal,
            setIsNeededRefresh,
            setModalHeader } = props;
    return(
        <div className={styles.main}>
            <CustomerContainer customers = {customers}
                                onClickHandler = {onClickHandler}
                                currentCustomer={currentCustomer}
                                customerDelClickHandler={customerDelClickHandler}
                                openModal={openModal}
                                setModalHeader={setModalHeader}
                                />
            <AccountContainer currentCustomer = {currentCustomer}
                                accountDelClickHandler={accountDelClickHandler}
                                openModal={openModal}
                                setModalHeader={setModalHeader}
                               />
        </div>
    )
}

export default Main;