import React from "react";
import Customer from "../customer/Customer.js";
import styles from "./CustomerContainer.module.scss";
import NewCustomerForm from "../newCustomerForm/NewCustomerForm.js";

function CustomerContainer(props) {
    const {customers, onClickHandler, currentCustomer, customerDelClickHandler, openModal, setModalHeader} = props;
    console.log(customers);



    return(
        <div className={styles.customerContainer}>
            <div className={styles.customerHeader}>
                <h5>Customers</h5>
                <button onClick = {()=>{openModal(<NewCustomerForm/>); setModalHeader("Create new Customer")}} className={styles.addbutton}>Add new</button>
            </div>
            <div>
                {customers.length !== null
                && customers.length > 0
                && customers.map(({id, name, email, age}) =>
                    <Customer key = {id}
                              name = {name}
                              email = {email}
                              age = {age}
                              onClickHandler={()=> {onClickHandler(id)}}
                              currentCustomerId={currentCustomer.id}
                              customerDelClickHandler={()=> customerDelClickHandler(id)}
                              id={id}/>)}
            </div>
        </div>
    )
}

export default CustomerContainer;