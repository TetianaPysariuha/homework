import React from "react";
import styles from "./Account.module.scss";

function Account (props) {
    const{number, currency, balance, accountDelClickHandler} = props;
    return (
        <div className={styles.account}>
            <p>{number}</p>
            <p>{currency}</p>
            <p>{balance}</p>
            <button className={styles.addbutton} onClick={accountDelClickHandler}>Delete</button>
        </div>
    )
}

export default Account;