import React from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal'

class App extends React.Component {

  state = {
    isOpenModal: false,
    actions: null,
    header: null,
    closeButton: true,
    text: null,
    closeModaleHandle: null,
  }

closeModal = () =>{
  this.setState({isOpenModal: false});
}

openModal = (header = null, closeButton= true, text= null, actions= null, closeModaleHandle= null) => {
  this.setState({
    isOpenModal: true,
    actions: actions,
    header: header,
    closeButton: closeButton,
    text: text,
    closeModaleHandle: closeModaleHandle,
  })
}

render() {  
  const { isOpenModal, header, closeButton, text, actions, closeModaleHandle } = this.state;
  return (
    <div className="App">
      <Button backgroundColor = 'lightred' 
              color="black"
              handleClick = {() => {this.openModal("Do you want to delete this file?", 
                                                    true,
                                                    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur autem eius accusamus quibusdam corporis harum hic praesentium consectetur cum, esse, quo rerum commodi vitae laborum magnam debitis! Necessitatibus, dolorum nulla.',
                                                      <div className='buttons'>
                                                        <Button handleClick = {this.closeModal} backgroundColor = 'rgba(255, 0, 0, 0.3)' color="white">OK</Button>
                                                        <Button handleClick = {this.closeModal} backgroundColor = 'rgba(255, 0, 0, 0.3)' color="white">Cansel</Button>
                                                      </div>,
                                                      this.closeModal,
                                                    )}}>Open first modal</Button>
      
      <Button backgroundColor = 'lightgreen' 
              color="black"
              handleClick = {() => {this.openModal("Do you want to create new file?", 
                                                    false,
                                                    'The new file with such name exist. Do you want to rewrite file?',
                                                      <div className='buttons'>
                                                        <Button handleClick = {this.closeModal} backgroundColor = 'rgba(255, 0, 0, 0.3)' color="white">Rewrite</Button>
                                                        <Button handleClick = {this.closeModal} backgroundColor = 'rgba(255, 0, 0, 0.3)' color="white">Create with new Name</Button>
                                                        <Button handleClick = {this.closeModal} backgroundColor = 'rgba(255, 0, 0, 0.3)' color="white">Cansel</Button>
                                                      </div>,
                                                      this.closeModal,
                                                    )}}>Open second modal</Button>
      
      {isOpenModal && <Modal header = {header} 
                              closeButton = {closeButton} 
                              text = {text}
                              actions = {actions}
                              closeModaleHandle = {closeModaleHandle}>
                      </Modal>}

    </div>
  );
}

}

export default App;
