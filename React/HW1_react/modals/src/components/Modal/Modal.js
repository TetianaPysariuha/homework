import React from "react";
import styles from './Modal.module.scss';
import Button from "../Button/Button";

class Modal extends React.PureComponent {

render() {
    const { header, text, closeButton, actions, closeModaleHandle } = this.props;

    
    return <div className={styles.container}>
                <div onClick={closeModaleHandle} className={styles.containerBackground}></div>
                <div className={styles.content}>
                    <div className={styles.modalHeader}>
                        <h1>{ header }</h1>
                        {closeButton && <Button handleClick = {closeModaleHandle} backgroundColor = 'rgba(255, 0, 0, 0.3)' color = "white">X</Button>}
                    </div>
                    <p className={styles.text}>{ text }</p>
                    { actions }
                </div> 
            </div>
}}

export default Modal;