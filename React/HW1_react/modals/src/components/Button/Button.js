import React from "react";
import styles from './Button.module.scss'

class Button extends React.PureComponent {

    render() {
        const { children, handleClick, backgroundColor, color } = this.props;
        return <button type = "button" 
                        onClick = { handleClick } 
                        style = {{ backgroundColor: backgroundColor, color: color }} 
                        className = {styles.btn}>{ children }</button>
    }
};

export default Button;