import React from "react";
import styles from './Modal.module.scss';
import Button from "../Button/Button";
import PropTypes from 'prop-types';

function Modal (props) {
    const { modalProps: {header, text, closeButton, closeModaleHandle, confirmModalHandle, art}} = props;
    return <div className={styles.container}>
                <div onClick={closeModaleHandle} className={styles.containerBackground}></div>
                <div className={styles.content}>
                    <div className={styles.modalHeader}>
                        <h2>{ header }</h2>
                        {closeButton && <Button handleClick = {closeModaleHandle} backgroundColor = 'rgba(0, 0, 0, 0.3)' color = "white">X</Button>}
                    </div>
                    <p className={styles.text}>{ text }</p>
                    <div className={styles.btns}>
                        <Button handleClick = {() =>{confirmModalHandle(art); closeModaleHandle()}} backgroundColor = 'rgba(0, 0, 0, 0.2)' color="white" btnText ="OK" ></Button>
                        <Button handleClick = {closeModaleHandle} backgroundColor = 'rgba(0, 0, 0, 0.2)' color="white" btnText ="Cansel"></Button>
                    </div>
                </div> 
            </div>
    }


Modal.propTypes = {
/*     header: PropTypes.string, 
    text: PropTypes.string, 
    closeButton: PropTypes.bool, 
    closeModaleHandle: PropTypes.func, 
    confirmModalHandle: PropTypes.func, 
    art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, */
}

Modal.defaultProps = {
/*     header: "", 
    text: "", 
    closeButton: true, 
    closeModaleHandle: () => {}, 
    confirmModalHandle: () => {},  */
}

export default Modal;