import React from "react";
import styles from "./CartContainer.module.scss";
import CartItem from "../CartItem/CartItem"
import PropTypes from 'prop-types';

function CartContainer (props) {
    const {cartData, 
            handleClickDelFromCart, 
            increaseCountInCart, 
            decreaseCountInCart, 
            noElementText,
            totalAmount
        } = props;
    return(
        <div className={styles.cartContainer}>
            {cartData.length === 0 && <p className={styles.noItemsText}>{noElementText}</p>}
            {cartData.map(({imgSrc, name, color, count, art, price}) => <CartItem  key = {art} 
                                                                                    url={imgSrc} 
                                                                                    title ={name} 
                                                                                    color ={color} 
                                                                                    count = {count} 
                                                                                    art= {art} 
                                                                                    price = {price} 
                                                                                    handleClickDelFromCart={handleClickDelFromCart}
                                                                                    increaseCountInCart = {increaseCountInCart} 
                                                                                    decreaseCountInCart = {decreaseCountInCart}/>)}
            {cartData.length !== 0 && <p className={styles.totalAmount}><span>Всього:</span> <span>{totalAmount}</span></p>}
        </div>
    )
}

CartContainer.propTypes = {
    cartData: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired,
    handleClickDelFromCart: PropTypes.func
}

CartContainer.defaultProps = {
    sectionCardTitle: '',
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: '',
        price: null,
        color: null, 
        isFavorite: false,
        count: 0
    })),
    handleClickAddToCart: () => {}
}

export default CartContainer;