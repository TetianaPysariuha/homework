import React from "react";
import styles from './Container.module.scss';
import Card from "../Card/Card";
import PropTypes from 'prop-types';

function Container (props) {
    const {sectionCardTitle, dataCards, handleClickAddToCart, handleClickOnFavorite, noElementText} = props;
    return(
        <div  className={styles.container}>
            <h1>{sectionCardTitle}</h1>
            {dataCards.length === 0 && <p className={styles.noItemsText}>{noElementText}</p>}
            {dataCards.map(({name, price, imgSrc, art, color, isFavorite}) => 
                <Card key = {art} title= {name} price={price} url={imgSrc} art={art} color={color} isFavorite={isFavorite} handleClickAddToCart={handleClickAddToCart} handleClickOnFavorite={handleClickOnFavorite}/>)}
        </div>
    )
}

Container.propTypes = {
    sectionCardTitle: PropTypes.string,
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
    })).isRequired,
    handleClickAddToCart: PropTypes.func,
}

Container.defaultProps = {
    sectionCardTitle: '',
    handleClickAddToCart: () => {}
}

export default Container;