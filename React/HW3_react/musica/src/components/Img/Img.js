import React from "react";
import PropTypes from 'prop-types'

function Img (props) {
    const {url, alt, title} = props;
    return <img src={url} alt = {alt} title = {title}/>
}

Img.propTypes = {
    url: PropTypes.string.isRequired,
    alt: PropTypes.string,
    title: PropTypes.string
}

Img.defaultProps = {
    alt: "An Image",
    title: ''
}

export default Img;