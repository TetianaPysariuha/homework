import React from "react";
import styles from "./CartItem.module.scss";
import Img from '../Img/Img';
import PropTypes from 'prop-types';
import Button from "../Button/Button";

function CartItem (props) {
    const { url, title, color, count, art, price, handleClickDelFromCart, increaseCountInCart, decreaseCountInCart} = props;
    return(
        <div className={styles.cutrItem}>
            <Img url = {url} alt = {title}/>
            <span>{title} {color} (Арт. {art} )</span>
            <div className={styles.btnCount}>
                <Button btnText= "+" handleClick = {()=> {increaseCountInCart(art)}} backgroundColor = "rgba(0,0,0,0.2)" color = "rgba(0, 128, 50, 0.8)"/>
                <Button btnText= "-" handleClick = {()=> {decreaseCountInCart(art)}} backgroundColor = "rgba(0,0,0,0.2)" color = "rgba(0, 128, 50, 0.8)"/>
            </div>
{/*             <span>{count}</span>
            <span>x</span>
            <span>{price}</span>
            <span>=</span>
            <span>{count*price}</span> */}
            <p>{count}</p>
            <p>x</p>
            <p>{price}</p>
            <p>=</p>
            <p>{count*price}</p>
            <Button btnText= "delete" handleClick = {()=> {handleClickDelFromCart(art)}} backgroundColor = "rgba(0, 128, 50, 0.8)" color = "white"/>
        </div>
    )
}


CartItem.propTypes = {
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired, 
    art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
    color: PropTypes.string, 
    handleClickDelFromCart: PropTypes.func,
    count: PropTypes.number
}

CartItem.defaultProps = {
    color: null,
    count: 0,
    handleClickDelFromCart: () => {}
}

export default CartItem;