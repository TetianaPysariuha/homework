import React from "react";
import styles from "./Header.module.scss";
import Img from '../Img/Img';
import CartContainer from "../CartContainer/CartContainer"
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

function Header (props) {
        const {counterCart, counterFavorite} = props;
        return(
            <header className={styles.header}>
                <div className={styles.logo}>
                    <Img url = "./img/Logo.svg" alt = "Logo"/>
                    <NavLink to="/">CHAIRS</NavLink>
                </div>
                <div className={styles.navigation}>
                    <div className={styles.catalog}>
                        <NavLink to="/">
                            <Img url = "./img/catalog.svg" alt = "Catalog" title = 'Перейти до каталогу'/>
                        </NavLink>
                    </div>
                    <div className={styles.cart}>
                        <NavLink to="/cart">
                            <Img url = "./img/cart-outline.svg" alt = "Cart" title = 'Перейти до кошика'/>
                        </NavLink>
                        <p>{counterCart}</p>
                    </div>
                    <div className={styles.favorite}>
                        <NavLink to="/favorite">
                            <Img url = "./img/star-header.svg" alt = "Favorite" title = 'Перейти до вибраних'/>
                        </NavLink>
                        <p>{counterFavorite}</p>
                    </div>
                </div>
            </header>
        )
    }

CartContainer.propTypes = {
    cartData: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired,
    counterCart: PropTypes.number,
    counterFavorite: PropTypes.number,
    handleClickDelFromCart: PropTypes.func,
    showCart: PropTypes.func,
    isCartAvaible: PropTypes.bool
}

CartContainer.defaultProps = {
    sectionCardTitle: '',
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: '',
        price: null,
        color: null, 
        isFavorite: false,
        count: 0
    })),
    handleClickAddToCart: () => {},
    counterCart: 0,
    counterFavorite: 0,
    showCart: () => {},
    isCartAvaible: false
}


export default Header;