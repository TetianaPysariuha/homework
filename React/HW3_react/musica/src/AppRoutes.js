import React from 'react';
import { Routes, Route } from 'react-router-dom';
import CartPage from './pages/CartPage/CartPage';
import FavoritePage from './pages/FavoritePage/FavoritePage';
import MainPage from './pages/MainPage/MainPage';


function AppRoutes (props) {
    const {dataCards, 
            handleClickAddToCart, 
            handleClickOnFavorite, 
            cartData, 
            handleClickDelFromCart, 
            increaseCountInCart,
            decreaseCountInCart,
            totalAmount } = props;
    
    return(
        <Routes>
            <Route path = "/" element = {<MainPage sectionCardTitle = 'Розділ Меблі' 
                                                    dataCards = {dataCards} 
                                                    handleClickAddToCart = {handleClickAddToCart} 
                                                    handleClickOnFavorite={handleClickOnFavorite}
                                                    />}/>
            <Route path = "/cart" element = {<CartPage cartData = {cartData} 
                                                        handleClickDelFromCart = {handleClickDelFromCart}
                                                        increaseCountInCart = {increaseCountInCart}
                                                        decreaseCountInCart = {decreaseCountInCart}
                                                        totalAmount = {totalAmount}
                                                        />}/>
            <Route path = "/favorite" element = {<FavoritePage sectionCardTitle = 'Улюблені'
                                                    dataCards = {dataCards} 
                                                    handleClickOnFavorite={handleClickOnFavorite}
                                                    handleClickAddToCart = {handleClickAddToCart}/>}/>
        </Routes>
    )
}

export default AppRoutes;