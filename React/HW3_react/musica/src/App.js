import React, { useState, useEffect } from 'react';
import './App.css';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import AppRoutes from './AppRoutes';
import Modal from "./components/Modal/Modal"



function App() {

  const [data, setData] = useState([]);
  const [cartData, setCartData] = useState([]);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [counterCart, setCounterCart] = useState(0);
  const [counterFavorite, setCounterFavorite] = useState(0);
  const [modalProps, setModalProps] = useState({});
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    if (localStorage.getItem('data')) {
      const newData = JSON.parse(localStorage.getItem('data'));
      setData(newData);
      const counterFavorite = newData.filter((el) => el.isFavorite).length
      setCounterFavorite(counterFavorite);
    } else {
      (async () => {
        const newData = await fetch('./data.json').then(res => res.json());
        setData(newData);
        localStorage.setItem('data', JSON.stringify(newData));
      })();
    };
    if (localStorage.getItem('cartData')) {
      const newCartData = JSON.parse(localStorage.getItem('cartData'));
      setCartData(newCartData);
      let newCounterCart = 0;
      if (newCartData.length > 0) {
        let init = 0;
        newCounterCart = newCartData.reduce((acc, cur) => acc + cur?.count, init);
      };
      setCounterCart(newCounterCart);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('data', JSON.stringify(data));
  }, [data]);

  useEffect(() => {
    localStorage.setItem('cartData', JSON.stringify(cartData));
    let init = 0;
    const newCounterCart = cartData.reduce((acc, cur) => acc + cur?.count, init);
    setCounterCart(newCounterCart);
    setTotalAmount(countTotalAmountInCart());
  }, [cartData]);

  const openModal = (header, closeButton, text, closeModaleHandle, confirmModalHandle, art) => {
    setModalProps({
      header: header,
      closeButton: closeButton,
      text: text,
      closeModaleHandle: closeModaleHandle,
      confirmModalHandle: confirmModalHandle,
      art: art
    });
    setIsOpenModal(true);
  }

  const closeModal = () => {
    setIsOpenModal(false);
  }

  const countTotalAmountInCart = () => {
    const currCartData = [...cartData];
    let init = 0;
    const newTotalAmount = currCartData.reduce((acc, cur) => acc + cur?.count*cur?.price, init);
    return newTotalAmount
  }

  const handleClickAddToCart = (card) => {
    openModal("Додати до кошика?", 
              false, 
              "Товар: " + card?.title + " (" + card?.color +") " ,
              closeModal,
              confirmModalAddToCartHandle,
              card.art
              );
  }

  const confirmModalAddToCartHandle = (art) => {
    const index = getElIndexByArt(cartData, art);
    const currData = [...data];
    if (index === -1) {
      setCartData([...cartData, { ...currData[getElIndexByArt(currData, art)], count: 1 }]);
    } else {
      setCartData((prev) => {
        const newCartData = [...prev];
        newCartData[index].count += 1;
        return newCartData;
      });
    };
  }

  const increaseCountInCart = (art) => {
    const newCartData = [...cartData];
    const index = getElIndexByArt(newCartData, art);
    newCartData[index].count += 1;
    setCartData(newCartData);
  }

  const decreaseCountInCart = (art) => {
    const newCartData = [...cartData];
    const index = getElIndexByArt(newCartData, art);
    if (newCartData[index].count === 1) {
      return;
    };
    newCartData[index].count -= 1;
    setCartData(newCartData);
  }

  const handleClickDelFromCart = (art) => {
    const elemCart = cartData[getElIndexByArt(cartData, art)];
    openModal("Видалити з кошика?", 
              false, 
              "Товар: " + elemCart?.name + " (" + elemCart?.color +") " ,
              closeModal,
              confirmModalDelFromCartHandle,
              art
              );
  }

  const confirmModalDelFromCartHandle = (art) => {
    setCartData((current) => {
      const newCartData = [...current];
      const index = getElIndexByArt(newCartData, art);
      if (index === -1) {
        return;
      } else {
        newCartData.splice(index, 1);
      }
      return newCartData;
    });
  }

  const handleClickOnFavorite = (art) => {
    setData((current) => {
      const newData = [...current];
      const elemIndex = getElIndexByArt(newData, art);
      newData[elemIndex].isFavorite = (newData[elemIndex].isFavorite ? false : true);
      setCounterFavorite((prev) => prev + (newData[elemIndex].isFavorite ? +1 : -1));
      return newData;
    })
  }

  const getElIndexByArt = (arrData, art) => {
    return arrData.findIndex((element) => element.art === art)
  }


  return (
    <div className="App">
      <AppRoutes sectionCardTitle='Розділ Меблі'
        dataCards={data}
        handleClickAddToCart={handleClickAddToCart}
        handleClickOnFavorite={handleClickOnFavorite}
        cartData={cartData}
        handleClickDelFromCart={handleClickDelFromCart}
        closeModal={closeModal}
        increaseCountInCart={increaseCountInCart}
        decreaseCountInCart={decreaseCountInCart}
        openModal={openModal}
        totalAmount = {totalAmount}
      />
      {isOpenModal && <Modal modalProps={modalProps} />}
      <Header cartData={cartData} counterCart={counterCart} counterFavorite={counterFavorite} />
      <Footer />
    </div>
  );

}

export default App;
