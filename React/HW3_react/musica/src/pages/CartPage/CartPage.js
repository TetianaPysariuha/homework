import React from 'react';
import styles from "./CartPage.module.scss" ;
import CartContainer from '../../components/CartContainer/CartContainer';
import PropTypes from 'prop-types';

function CartPage (props) {
    const {cartData, 
            handleClickDelFromCart, 
            increaseCountInCart, 
            decreaseCountInCart,
            totalAmount
        } = props;

    return (
    <div className={styles.cartPage}>
       <h1>Кошик</h1>
       <CartContainer cartData={cartData}
                    noElementText={'Ви ще не вибрали жодного товару'}
                    handleClickDelFromCart={handleClickDelFromCart} 
                    increaseCountInCart = {increaseCountInCart} 
                    decreaseCountInCart = {decreaseCountInCart}
                    totalAmount = {totalAmount}/>
    </div>
    )
}

CartPage.propTypes = {
    cartData: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired, 
    handleClickDelFromCart: PropTypes.func, 
    increaseCountInCart: PropTypes.func,
    decreaseCountInCart: PropTypes.func,
    totalAmount: PropTypes.number
}

CartPage.defaultProps = {
    handleClickDelFromCart: () => {}, 
    increaseCountInCart: () => {},
    decreaseCountInCart: () => {},
    totalAmount: 0
}

export default CartPage;