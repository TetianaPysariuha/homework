import React from 'react';
import Container from '../../components/Container/Container';
import styles from "./MainPage.module.scss";
import PropTypes from 'prop-types';


function MainPage (props) {
    const { dataCards, 
            handleClickAddToCart, 
            handleClickOnFavorite,
            sectionCardTitle
          } = props;
    return (
        <div className={styles.mainPage}>
        <Container sectionCardTitle = {sectionCardTitle} 
                    dataCards = {dataCards} 
                    handleClickAddToCart = {handleClickAddToCart} 
                    handleClickOnFavorite={handleClickOnFavorite}/>
        </div>
    )
}


MainPage.propTypes = {
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired, 
    handleClickAddToCart: PropTypes.func, 
    handleClickOnFavorite: PropTypes.func
}

MainPage.defaultProps = {
    handleClickAddToCart: () => {}, 
    handleClickOnFavorite: () => {}
}

export default MainPage;