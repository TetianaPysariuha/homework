import React from 'react';
import styles from "./FavoritePage.module.scss";
import Container from '../../components/Container/Container';
import PropTypes from 'prop-types';

function FavoritePage(props) {
    const { dataCards,
            handleClickAddToCart, 
            handleClickOnFavorite,
            sectionCardTitle
        } = props;
    return (
        <div className={styles.favoritePage}>
            <Container sectionCardTitle = {sectionCardTitle}
                        noElementText = {'У Вас жодного товару поміченого як улюблене'}
                        dataCards = {dataCards.filter((elem) => elem.isFavorite===true)} 
                        handleClickOnFavorite={handleClickOnFavorite}
                        handleClickAddToCart = {handleClickAddToCart}/>
        </div>
    )
}

FavoritePage.propTypes = {
    dataCards: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        imgSrc: PropTypes.string.isRequired, 
        art: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool,
        count: PropTypes.number
    })).isRequired, 
    handleClickOnFavorite: PropTypes.func, 
    sectionCardTitle: PropTypes.string
}

FavoritePage.defaultProps = {
    sectionCardTitle: "", 
    handleClickOnFavorite: () => {}
}

export default FavoritePage;