1. Какие существуют типы данных в Javascript?
Существует две категории типов данных:
    * простые (примитивные)
    * объекты
К примитивным типам данных относятся:
    * Number - число в диапаззоне от -2 в степени 53 до 2 в степени 53, как целое так и с плавающей точкой.
    * BigInt - большие целые числа
    * String - строка
    * Bollen - логическая (включает только два значения true или false)
    * null - содержит только значение null (значение пустое)
    * underfined - содержит только значение underfined (значение не определено)
    * NaN - содержит только значение NaN (значение не является числом - Not a Number)
К объектным типам данных относятся:
    * object - объект
    * symbol - символ


2. В чем разница между == и === ?
    * == - оператор сравнения, где сравниваются значения с неявным приведением типов
    * === - более строгий оператор сравнения, где сравнивает как значение, так и тип данных (неявное приведение данных не используется)

3. Что такое оператор?
Оператор это символ(знак) или слово, который указывает какое действие необходимо выполнить над тем или иным объектом (в данном случае объект - то не тип данных). Например, математический знак + или * - это оператор. Он означает, что необходимо сложить или умножить числа (если брать как математический оператор). 
С разными объектами в JS один и тот же оператор может означать разные действия. Например, при использовании + перед срокой (переменной), данный оператор произведет преобразования типа данных из String в Number. Два знака ++ перед или после числа - инкриментирует число на 1.