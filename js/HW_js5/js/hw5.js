"use strict";

function getUserInformation (message) {
    let userInformation;
    do {
        userInformation= prompt(message, userInformation);
    } while (userInformation === '' || userInformation === null);
    return userInformation;
}

function createNewUser () {
    
    let firstName = getUserInformation('Please, enter your first name:');
    let lastName = getUserInformation('Please, enter your last name:');

    return {
        firstName,
        lastName,
        getLogin () {
            return (this.firstName.charAt(0)+this.lastName).toLowerCase();
        },
    }
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
