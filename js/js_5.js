//******************************************************************************************************
//Решение, где пользователь может отменить создание пользователя

function getUserInformation (message) {
    let userInformation;
    do {
        userInformation= prompt(message, userInformation);
    } while (userInformation === '');
    return userInformation;
}

function createNewUser () {
    
    let firstName = getUserInformation('Please, enter your first name:');
    if(!firstName /* === null */) {
        alert ('User creation has been aborted by user');
        return;
    }
    let lastName = getUserInformation('Please, enter your last name:');
    if(lastName === null) {
        alert ('User creation has been aborted by user');
        return;
    }

    return Object.create(
        {
            getLogin () {
                return (this.firstName.charAt(0)+this.lastName).toLowerCase();
            },
            setFirstName (newValueFirstName) {
                Object.defineProperty(this, 'firstName', {writable: true});
                this.firstName = newValueFirstName;
                Object.defineProperty(this, 'firstName', {writable: false});
            },
            setLastName (newValueLastName) {
                Object.defineProperty(this, 'lastName', {writable: true});
                this.lastName = newValueLastName;
                Object.defineProperty(this, 'lastName', {writable: false});
            },
        },
        {
            firstName:{
                value: firstName,
                enumerable: true,
                configurable: true,
            },
            lastName:{
                value: lastName,
                enumerable: true,
                configurable: true,
            }
        });
};

const newUser = createNewUser();
console.log(newUser);

if (newUser){
    newUser.setFirstName("Tetiana");
    newUser.setLastName("Pysariuha");
    console.log(newUser.getLogin());
    console.log(newUser);
};


