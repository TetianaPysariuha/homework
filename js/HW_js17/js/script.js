function receiveUserDataString(massage) {
    let result;
    do {
        result = prompt(massage);
    } while (result?.trim() === '');
    return result;
};

function receiveUserDataNumber(massage) {
    let result;
    do {
        result = prompt(massage);
    } while (result?.trim() === '' || isNaN(+result));
    return result;
};

function createStudent () {
    let name;
    let lastname;
    const tabel = {};
    let subject;
    let mark;

    if (!(name = receiveUserDataString('Enter your name, please:'))) {
        return;
    };
    if (!(lastname = receiveUserDataString('Enter your lastname, please:'))) {
        return;
    };

    do{
        subject = receiveUserDataString('Enter a subject:');
        if (subject) {
            mark = receiveUserDataNumber(`Enter a mark for ${subject}:`);
        };
        if(subject && mark) {
            tabel[subject] = mark;
        };
    } while(subject && mark);
    
    return newStudent = {
        name,
        lastname,
        tabel,
    };
};

function checkProgress(someStudent) {
    let IsBadMark = false;
    let sumMarks = 0;
    let countSubjects = 0;
    for (let key in someStudent.tabel) {
        if(someStudent.tabel[key] < 4) {
            IsBadMark = true;
        };
        sumMarks += +someStudent.tabel[key];
        countSubjects ++;
    };
    if(countSubjects === 0) {
        return;
    }; 
    if (!IsBadMark) {
        alert('Студент переведено на наступний курс');
    };
    if ( (sumMarks / countSubjects) > 7) {
        alert('Студенту призначено стипендію');
    };
}

const student = createStudent();
if (student) {
    checkProgress(student);
};
