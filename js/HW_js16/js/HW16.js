let userNumberF0;
let userNumberF1;
let userNumberN;

function receiveUserChoice (message, defaultChoice) {
    return prompt(message, defaultChoice);
}

function isNumberApropriate(userNumber) {
    return !(isNaN(+userNumber) || (userNumber !== '0' && +userNumber===0) || Math.trunc(+userNumber) !== +userNumber);
}
function culcFibonachi(numberF0, numberF1, sequenceNumber, currentSequenceNumber = 3) {
    if (Math.abs(sequenceNumber) === currentSequenceNumber) {
        return (sequenceNumber > 0 ? (numberF0 + numberF1) : (numberF0 - numberF1));
    } else {
       /*  console.log( (sequenceNumber > 0 ? (numberF0 + numberF1) : (numberF0 - numberF1)) ); */
        return culcFibonachi(numberF1, (sequenceNumber > 0 ? (numberF0 + numberF1) : (numberF0 - numberF1)), 
        sequenceNumber, ++currentSequenceNumber);
    }
}

do{
    userNumberF0 = receiveUserChoice("Enter, pls, the number F0:", userNumberF0);
} while (!isNumberApropriate(userNumberF0));

do{
    userNumberF1 = receiveUserChoice("Enter, pls, the number F1:", userNumberF1);
} while (!isNumberApropriate(userNumberF1));

do{
    userNumberN = receiveUserChoice("Enter, pls, the number n:", userNumberN);
} while (!isNumberApropriate(userNumberN));


alert(`With F0 = ${userNumberF0}, F1 = ${userNumberF1}, and n = ${userNumberN}, 
the number of Fibonacci is: ${culcFibonachi(+userNumberF0, +userNumberF1, +userNumberN)}`);