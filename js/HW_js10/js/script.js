
document.querySelector('.tabs').addEventListener('click', (event) => {
    const targetTab = event.target.dataset.tab;
    if(targetTab) {
        document.querySelectorAll('.active').forEach(elem => elem.classList.remove('active'));
        event.target.classList.add('active');
        document.querySelector(`.tabs-content li[data-tab = ${targetTab}]`).classList.add('active');
    }
});