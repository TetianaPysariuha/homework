const colorChange = document.querySelector('.change-color');

if(localStorage.getItem('colorScheme')) {
    document.head.insertAdjacentHTML('beforeend', `<link data-scheme = 'scmeme1' rel="stylesheet" href="./css/scheme1.css">`)
}

colorChange.addEventListener('click', (event) => {
    const scheme = localStorage.getItem('colorScheme');
    if(scheme) {
        localStorage.removeItem('colorScheme');
        document.head.querySelector('link[data-scheme = scmeme1]').remove();
    } else {
        localStorage.setItem('colorScheme', 'scmeme1');
        document.head.insertAdjacentHTML('beforeend', `<link data-scheme = 'scmeme1' rel="stylesheet" href="./css/scheme1.css">`)
    };
});

