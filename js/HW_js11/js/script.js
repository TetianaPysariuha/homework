const form = document.querySelector('.password-form');
const password = form.querySelector('#password');
const confermPassword = form.querySelector('#confirm-password');

form.addEventListener('click', (event) =>{
    if(event.target.dataset.eye) {
/*         console.log(event.target.atributes.type); */
        const input = event.target.parentNode.querySelector('input');
        input.type = (input.type === 'password' ? 'text' : 'password');
        event.target.classList.toggle('fa-eye');
        event.target.classList.toggle('fa-eye-slash');
    };
});

form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (password.value ===confermPassword.value && password.value.trim() !=='') {
        confermPassword.parentNode.querySelector('.error-inform').style.display = 'none';
        alert('You are welcome!');
        form.reset();
    } else {
        confermPassword.parentNode.querySelector('.error-inform').style.display = 'block';
    };
});

form.addEventListener('input', (event) => {
    if(event.target.id === 'password' || event.target.id === 'confirm-password') {
        confermPassword.parentNode.querySelector('.error-inform').style.display = 'none';
    };
});