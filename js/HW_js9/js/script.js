
function createList (someArray, parentElement = document.body) {
    const newArray = someArray.map((elem) => `<li class="list-element">${elem}</li>`);
    parentElement.insertAdjacentHTML('beforeend',`<ul class="wrapper">${newArray.join('')}</ul>`)
}

const arrayExample = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
createList(arrayExample, document.querySelector('.container'));

const arrayTest = ["1", "2", "3", "sea", "user", 23];
createList(arrayTest);