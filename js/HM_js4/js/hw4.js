let userOperation;
let userFirstNumber;
let userSecondNumber;

function receiveUserChoice (message, defaultChoice) {
    return prompt(message, defaultChoice);
}
function isNumberApropriate(userNumber) {
    return !(isNaN(+userNumber) || (userNumber !== '0' && +userNumber===0) || Math.trunc(+userNumber) !== +userNumber);
}

function calculateOperation (numberFirst, numberSecond, operation) {
    switch (operation) {
        case '+':
            return numberFirst + numberSecond;
        case '-':
            return numberFirst - numberSecond;
        case '*':
            return numberFirst * numberSecond;
         case '/':
            return numberFirst / numberSecond;
        default:
            console.log ("Operation type is not like '+' or '-' or '*' or '/'");
            break;
    }
}

do{
    userOperation = receiveUserChoice("Enter, pls, math operation (can be '+' or '-' or '*' or '/'):", userOperation);
} while (userOperation !== '+' && userOperation !== '-' && userOperation !== '*' && userOperation !== '/');

do{
    userFirstNumber = receiveUserChoice("Enter, pls, the first number:", userFirstNumber);
} while (!isNumberApropriate(userFirstNumber));

do{
    if(userOperation==='/') {
        userSecondNumber = receiveUserChoice("Second number should not be 0 for division! Enter, pls, the second number:", 
        userSecondNumber);
    } else {
        userSecondNumber = receiveUserChoice("Enter, pls, the second number:", userSecondNumber);
    }
} while (!isNumberApropriate(userSecondNumber) || (userOperation==='/' && +userSecondNumber===0));


console.log (calculateOperation (userFirstNumber, userSecondNumber, userOperation));