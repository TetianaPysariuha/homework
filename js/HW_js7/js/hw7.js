"use strict";

const arrWithDiffDataTypes = [[1,5,20], 50, 'check', null, undefined, true, {userName: 'John', userAge: 29,}];
const arrAllDataTypes = ['Number', 'undefined', 'null', 'boolean', 'array', 'string', 'object'];

function checkDataType(someData) {
    if (typeof someData!== 'object') {
        return (typeof someData);
    } else if (someData === null) {
        return 'null';
    } else if (Array.isArray(someData)) {
        return 'array';
    } else {
        return 'object';
    };
};

function filterBy(someArray, dataType) {
    return someArray.filter(elem => checkDataType(elem).toLowerCase() !==  dataType.toLowerCase());
};

console.log(arrWithDiffDataTypes);

arrAllDataTypes.forEach (type => {
    console.log(`without ${type} result is:`);
    console.log(filterBy (arrWithDiffDataTypes, type));
});
