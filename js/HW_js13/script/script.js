const driveBtnsCollection = document.querySelectorAll('.btn-drive');
const sliderImgCollection = document.querySelectorAll('.image-to-show');
let curentImg = 0;
let intervalTimerIndex = 0;

function sliderActivity() {
    sliderImgCollection[curentImg].classList.remove('active');
    curentImg = (++curentImg+sliderImgCollection.length)%sliderImgCollection.length;
    sliderImgCollection[curentImg].classList.add('active');
};

intervalTimerIndex = setInterval(sliderActivity, 3000);

driveBtnsCollection.forEach( elem => {
    elem.addEventListener('click', (event) => {
        driveBtnsCollection.forEach( element => element.classList.toggle('active'));
        if(intervalTimerIndex) {
            clearInterval(intervalTimerIndex);
            intervalTimerIndex = 0;
        } else {
            intervalTimerIndex = setInterval(sliderActivity, 3000);
        };
    });
});