//Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const allParagraf = document.getElementsByTagName('p');
for (let elem of allParagraf) {
    elem.classList.add('background-color-ff0000');
}

//Знайти елемент із id="optionsList". Вивести у консоль. 
//Знайти батьківський елемент та вивести в консоль. 
//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsListElement = document.querySelector('#optionsList');
console.log(optionsListElement);
console.log(optionsListElement.parentNode);
optionsListElement.childNodes.forEach(elem =>{
    console.log(`Node Name: ${elem.nodeName}; Node Type: ${elem.nodeType}`);
});

//Встановіть в якості контента елемента з класом testParagraph наступний параграф -
//This is a paragraph

const testParagraphElement = document.querySelector('#testParagraph');
console.log(testParagraphElement.innerText);
testParagraphElement.innerText = 'This is a paragraph'

//Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
//Кожному з елементів присвоїти новий клас nav-item.

const mainHeaderElement = document.querySelector('.main-header');
mainHeaderElement.childNodes.forEach(child => {
    console.log(child);
    child.className ='nav-item';
});

//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitleElements = document.querySelectorAll('.section-title');
sectionTitleElements?.classList?.remove('section-title');
