const btnContainer = document.querySelector('.btn-wrapper');
const btnCollection = document.querySelectorAll('.btn');

document.addEventListener('keyup', (event) => {
    const keyTarget = btnContainer.querySelector(`.btn[data-key = ${event.key}]`);
    if(keyTarget) {
        btnContainer.querySelector('.blue')?.classList.remove('blue');
        keyTarget.classList.add('blue');
    };
});

