
function cloneObjectDeep(someObject) {
    if (typeof someObject !== 'object' || someObject === null) {
        return someObject;
    };
    const newObject = Array.isArray(someObject) ? [] : {};
    for (let key in someObject) {
        if (typeof someObject[key] !== 'object') {
            newObject[key] = someObject[key];
        } else {
            newObject[key] = cloneObjectDeep(someObject[key]);
        };
    };
    return newObject;
};

const testObject = [10,20,30,
                    [11,22],
                    {name: 'ddsss', age: 20, sdsad: [55,33]},
                    50,
                 function write() {
                    console.log('Hello');
                }];
const newObject = cloneObjectDeep(testObject);
testObject[4].age = 50;

console.log(newObject);
console.log(testObject);
console.log(cloneObjectDeep(undefined));
console.log(cloneObjectDeep(null));
console.log(cloneObjectDeep(50));
