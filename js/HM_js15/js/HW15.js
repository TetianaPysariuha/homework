let receivedUserNumber;

function isNumberApropriate(userNumber) {
    return !(isNaN(+userNumber) || (userNumber !== '0' && +userNumber===0) || Math.trunc(+userNumber) !== +userNumber);
}

function culcFuctorial(someNumber) {
    if (someNumber===0 || someNumber===1){
        return 1;
    } else {
        return someNumber*culcFuctorial (someNumber - 1);
    }
}

do{
    receivedUserNumber = prompt("Enter, pls, the number:", receivedUserNumber);
} while (!isNumberApropriate(receivedUserNumber));

console.log (culcFuctorial(+receivedUserNumber));