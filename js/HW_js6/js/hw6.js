"use strict";

function getUserInformationString (message) {
    let userInformation;
    do {
        userInformation= prompt(message, userInformation);
    } while (userInformation === '' || userInformation === null);
    return userInformation;
}

function getUserInformationDate (message) {
    let userInformation = 'DD.MM.YYYY';
    let day;
    let month;
    let year;
    
    do {
        userInformation= prompt(message, userInformation);
    } while (isNaN(userInformation.slice(0, 2)) || 
            isNaN(userInformation.slice(3, 5)) ||
            isNaN(userInformation.slice(6)) || 
            userInformation === null || 
            userInformation ==='' ||
            userInformation.slice(2, 3) !== '.' ||
            userInformation.slice(5, 6) !== '.' );

    day = userInformation.slice(0, 2);
    month = userInformation.slice(3, 5);
    year = userInformation.slice(6);
    return (new Date(year, month-1, day));
}

function createNewUser () {
    
    const firstName = getUserInformationString('Please, enter your first name:');
    const lastName = getUserInformationString('Please, enter your last name:');
    const birthDay = getUserInformationDate('Please, enter your birthday (format DD.MM.YYYY):');

    return {
        firstName,
        lastName,
        birthDay,
        getLogin () {
            return (this.firstName.charAt(0)+this.lastName).toLowerCase();
        },
        getAge() {
            return Math.trunc(((new Date())-this.birthDay)/(1000*60*60*24*365));
        },
        getPassword() {
            return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthDay.getFullYear());
        },
    }
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());