let userChoise;

do{
    userChoise = prompt('Please, enter the number of choiсe: 1 - number multiple of 5; 2 - prime numbers.', userChoise);
} while (+userChoise !== 1 && +userChoise !== 2);

switch(userChoise){
    case '1':
        let userNumber;
        let countNumbers = 0;   

        do{
            userNumber = prompt('Please, enter the whole number more than 0:', userNumber);
        } while (isNaN(+userNumber) || +userNumber <= 0 || Math.trunc(+userNumber) !== +userNumber);
        
        for (let i = userNumber; i > 0; i--){
            if(i % 5 === 0){
                console.log(i);
                countNumbers++;
            }
        }
        
        if (countNumbers === 0) {
            console.log("Sorry, no numbers");
        }

        break;
    case '2':
        let userFirstNumber;
        let userSecondNumber;

        do{
           userFirstNumber = prompt('Please, enter the first whole number more than 0:', userFirstNumber);
        } while (isNaN(+userFirstNumber) || +userFirstNumber <= 0 || Math.trunc(+userFirstNumber) !== +userFirstNumber);
        
        do{
            userSecondNumber = prompt('Please, enter the first whole number more than 0:', userSecondNumber);
         } while (isNaN(+userSecondNumber) || +userSecondNumber <= 0 || Math.trunc(+userSecondNumber) !== +userSecondNumber);

         const smallerNumber = +userFirstNumber > +userSecondNumber ? +userSecondNumber : +userFirstNumber;
         const biggerNumber =  +userFirstNumber > +userSecondNumber ? +userFirstNumber : +userSecondNumber;
         
         for (let i = smallerNumber; i <= biggerNumber; i++){
            let countCasesDivisions = 0;
            for (let j = 2; j < i; j++){
                if (i % j === 0) {
                    countCasesDivisions++;
                } 
            }
            if (countCasesDivisions === 0) {
                console.log(i);
            }
         }

         break;
    default:
        console.log("Something was wrong with your choice of action.");
}


